import time
import random
import pyttsx3 as ts

engine=ts.init()
def attendance_list(students):
    absent=0
    present=0
    absentees=[]
    for i in students:
        time.sleep(1)
        engine.say(i)
        print(i)
        engine.runAndWait()
        try:
            answer=input('Present/Absent:').lower()
            while answer[0]=='a' or answer[0]=='p':
                if answer.startswith('p'):
                    present=present+1
                else:
                    absent=absent+1
                    absentees.append(i)
                break
            else:
                answer=input('Invalid Entry, enter again p/a: ').lower()

        except Exception as e:
            print('Invlaid entry',e)

    return absentees

def attendance_record(s_list):
    attendance=dict()
    for i in range(len(s_list)):
        attendance[i+1]=s_list[i]
    return attendance

students_list='Abhishek Rohan Rahul Tarun Ronak '.split()#Harsh Gaurav Shradha Harsha Devraj Devdutt Virat Bhavesh Satvik Vivek Prerana Akanksha Manoj Varun Vijay Yashas Divya Vaishnavi Sidhi Rashi Yumeko'.split()
students_list.sort()

total=len(students_list)
absent_student=attendance_list(students_list)

print('*'*45)
print('Total Number of absentees:',len(absent_student),'\n')
engine.say('Total Number of absentees:')
engine.say(len(absent_student))
print('Total number of students present:',total-len(absent_student))
engine.say('Total Number of students present:')
engine.say(total-len(absent_student))
print(' \n  Absent Student: \n')
for i in range(len(absent_student)):
    print(i+1,absent_student[i])
print('*'*45)
engine.runAndWait()
response=input('Do you want to view Students list?(y/n) ').lower()
if response.startswith('y'):
    students=attendance_record(students_list)
    for roll_no,student in students.items():
        print(roll_no,':',student)
else:
    print('Thank you!')