Text to Speech Attendance Python Code
    
Author : Tarun R Jain
    
Requirements: Text to Speech Library: pyttsx3

To install type this command: pip install pyttsx3

Basic commands and Tutorial for Python text to speech package

import pyttsx3 as ts

engine=ts.init() #initialize the text to speech in program
engine.say("Type here text which should be repeated in speech")
engine.runAndWait() #this line is useful to receive the output

Thank you
